#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

typedef struct Fig_t {
	int a;
	int b;
} Fig_t;

void leerFiguras(Fig_t *fs, int size){
	int i = 0;
	for (i = 0; i < size; i++){
		fs[i].a = i;
		fs[i].b = i + 1;
	}
}

void imprimirFiguras(Fig_t *fs, int size){
	int i = 0;
	for (i = 0; i < size; i++){
		printf("a: %d, b: %d\n", fs[i].a, fs[i].b);
	}
}

void imprimirFigurasC(Fig_t fs[], int size){
	int i = 0;
	for (i = 0; i < size; i++){
		printf("a: %d, b: %d\n", fs[i].a, fs[i].b);
	}
}

int main(int argc, char *argv[]) {
	Fig_t fs[10];
	leerFiguras(fs, 10);
	imprimirFigurasC(fs, 10);
	return 0;
}