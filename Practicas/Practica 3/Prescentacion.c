/*Programa que imprime una presentacion de los datos del alumno*/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    char    inicial1        = ' ';
    char    inicial2        = ' ';
    int     edad            = 0;
    int     matricula       = 0;
    int     semestre        = 0;
    float   calificacion1   = 0.0f;
    float   calificacion2   = 0.0f;
    float   calificacion3   = 0.0f;
    float   calificacion4   = 0.0f;
    float   calificacion5   = 0.0f;

    /* *******************************************************************************

        Atencion, estos codigos son de ejemplo y tienen errores a proposito

    ***********************************************************************************/

    printf("Ingrese su primera inicial: ");
    scanf("%c", &inicial1);

    printf("Ingrese su primera inicial: ");
    scanf(" %c", &inicial2);

    printf("Ingrese su matricula: ");
    scanf("%d", &matricula);

    printf("Ingrese el semestre del alumno: ");
    scanf("%d", &semstre);

    printf("Ingrese la edad del alumno: ");        
    scanf("%c", &edad);

    printf("Ingrese la calificacion no. 1: ");
    scanf("%f", &calificacion1);
    
    printf("Ingrese la calificacion no. 2: ");    
    scanf("%f", &calificacion1);

    printf("Ingrese la calificacion no. 3: ");        
    scanf("%f", &calificacion1);

    printf("Ingrese la calificacion no. 4: ");    
    scanf("%f", &calificacion1);

    printf("Ingrese la calificacion no. 5: ");        
    scanf("%f", &calificacion1);

    promedio = (calificacion1 + calificacion2 + calificacion3 + calificacion4 + calificacion5) / 5;

    printf("CIniciales: %c%c \n", inicial1, inicial2);
    printf("OMatricula: %d \n", matricula);
    printf("PSemestre: %d \n", semestre);
    printf("IEdad: %d \n", edad);
    printf("ACalificaciones: %.2f, %.2f, %.2f, %.2f\n", calificacion1, calificacion2, calificacion3, calificacion4, calificacion5);
    printf("Promedio de calificaciones: %.2f \n", promedio);
    
    return 0;
}
