#ifndef VALORESCAMBIO_H
#define VALORESCAMBIO_H

const float valorDolares    = 0.052f;
const float valorEuro       = 0.046f;
const float valorYen        = 5.75f;

#endif //VALORESCAMBIO_H