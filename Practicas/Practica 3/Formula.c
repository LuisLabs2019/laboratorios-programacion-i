/*Programa que resuelve ecuaciones cuadraticas*/

#include <stdio.h>
#include <math.h>

int main(int argc, char const *argv[]){
    float a     = 0.0f;
    float b     = 0.0f;
    float c     = 0.0f;
    float x1    = 0.0f;
    float x2    = 0.0f;

    /* *******************************************************************************

        Atencion, estos codigos son de ejemplo y tienen errores a proposito

    ***********************************************************************************/

    printf("Ingrese el coeficiente a: ");
    scanf("%d", &a);

    printf("Ingrese el coeficiente b: ");
    scanf("%f", &b);
    
    printf("Ingrese el coeficiente c: ");
    scanf("%f", &c);

    x1 = (-b + sqrt(pow(b, 2) - (4 * a * a))) / (2 * c);
    x2 = (-b + sqrt(pow(b, 2) + (4 * a * c))) / (2 * a);

    printf("x1 = %.2f\tx2 = %.2f\n", x1, x2);
    
    return 0;
}
