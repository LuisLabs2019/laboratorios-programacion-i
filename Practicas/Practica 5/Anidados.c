#include <stdio.h>
#include <stdlib.h>

// Lee datos en un arreglo
void inputData(double* array, const int* size);

// Obtiene el promedio de un arreglo
double getAverage(const double* array, const int* size);

// Cuenta materias reprobadas
int getFailed(const double* array, const int* size);

// Califica al alumno
void gradeStudent(const double* array, const int* size);

int main(int argc, char const *argv[]) {
    // Longitud del arreglo
    const int ARRAY_SIZE = 5;

    // Definicion del arreglo
    double* calificaciones = malloc(sizeof(double[ARRAY_SIZE]));

    // Llamada a la funcion para ingresar los datos
    inputData(calificaciones, &ARRAY_SIZE);

    // Llamada a la funcion para calificar al alumno
    gradeStudent(calificaciones, &ARRAY_SIZE);

    // Libera el espacio de memoria ocupado por "double* calificaciones"
    free(calificaciones);
  
    return 0;
}

void inputData(double* array, const int* size){
    for(int i = 0; i < *size; i++) {
        printf("Ingrese la calificacion num. %d: ", i + 1);
        scanf("%lf", &array[i]);
    }
}

double getAverage(const double* array, const int* size){
    double avg = 0.0f;
    for (int i = 0; i < *size; i++) avg += array[i];
    return avg / *size;
}

int getFailed(const double* array, const int* size){
    int failed = 0;
    for(int i = 0; i < *size; i++) if (array[i] < 70) failed++;
    return failed;
}

void gradeStudent(const double* array, const int* size){
    const double    APPROVING_GRADE     = 70.0f;
    const double    HONOR_GRADE         = 90.0f;
    const int       MAX_FAIL            = 3;
    double promedio = getAverage(array, size);
    printf("Promedio = %.2lf\n", promedio);
    if (promedio >= APPROVING_GRADE){
        if (promedio > HONOR_GRADE)
            printf(
                "El alumno ha aprobado el semestre con promedio destacado: %.2lf\n", 
                promedio
            );
        else
            printf(
                "El alumno ha aprobado el semestre con buenas calificaciones: %.2lf\n", 
                promedio
            );
    } else {
        int failed = getFailed(array, size);
        if (failed > MAX_FAIL)
            printf(
                "El alumno ha reprobado el semestre: %.2lf\n", 
                promedio
            );
        else
            printf(
                "El alumno ha pasado el semestre con malas calificaciones: %.2lf\n", 
                promedio
            );  
    }
}