#include <stdio.h>


void imprimirGlobal();
/* void imprimirLocal(int local); */
/* void imprimirLocal(); */

// Variable Global
int global = 5;

int main(int argc, char const *argv[]) {
    // Variable local
    int local = 2;

    imprimirGlobal();
    /* imprimirLocal(); */
    /* imprimirLocal(local); */

    return 0;
}

void imprimirGlobal() {
    printf("global = %d\n", global);
}

/* void imprimirLocal(int local){
    printf("local = %d\n", local);
} */

/* void imprimirLocal(){
    printf("local = %d\n", local);
} */
