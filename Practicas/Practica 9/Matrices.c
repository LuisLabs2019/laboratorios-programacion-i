#include <stdio.h>

int main(int argc, char const *argv[]) {
    int mat[3][3];
    int cont = 0;
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            mat[j][i] = cont;
            cont++;
        }
    }
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            printf("%d, ", mat[j][i]);
        }
        printf("\n");
    }
    return 0;
}
