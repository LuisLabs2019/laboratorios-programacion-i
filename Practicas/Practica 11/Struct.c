#include <stdio.h>
#include <stdlib.h>

typedef struct Rectangulo_t {
	double alto;
	double ancho;
};

typedef struct Rectangulo_t Rectangulo;

int main(int argc, char *argv[]) {
	Rectangulo rectangulo;
	
	//Rectangulo arreglo[10];
	//arreglo[i].alto
	
	rectangulo.alto = 12;
	rectangulo.ancho = 10;
	double area = rectangulo.alto * rectangulo.ancho;
	printf("El area del rectangulo con alto %.2f y ancho %.2f es: %.2f\n", rectangulo.alto, rectangulo.ancho, area);
	return 0;
}