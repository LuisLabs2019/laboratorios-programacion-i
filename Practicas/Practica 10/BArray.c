#include <stdio.h>
#include <stdlib.h>

void fillArray(char array[][256], const size_t CANT_STR, const size_t STR_SIZE, const char* msg);
void printArray(const char array[][256], const size_t CANT_STR);

int main(int argc, char const *argv[]) {
    size_t CANT_STR = 5;
    size_t STR_SIZE = 256;
    char alumnos[CANT_STR][STR_SIZE];
    fillArray(alumnos, CANT_STR, STR_SIZE, "Ingrese una cadena: ");
    printArray(alumnos, CANT_STR);
    return 0;
}

void fillArray(char array[][256], const size_t CANT_STR, const size_t STR_SIZE, const char* msg){
    for(size_t i = 0; i < CANT_STR; i++) {
        printf("%s", msg);
        fgets(array[i], sizeof(array[i]), stdin);
    }
}

void printArray(const char array[][256], const size_t CANT_STR){
    for(size_t i = 0; i < CANT_STR; i++) {
        printf("%s", array[i]);
    }
}