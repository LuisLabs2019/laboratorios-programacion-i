#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int contador = 0;
	//int contador = 10;
	int aumento = 0;
	int opcion = 0;
	
	// while( 'condición' )
	// Si la condición es cierta se ejecutara el codigo
	// Si la condicion no es verdadera al inicio el codigo no se ejecuta
	
	while(contador < 10){
		printf("contador = %d\n", contador);
		printf("Desea sumar al contador? (1 - Si, 2- No): ");
		scanf("%d", &opcion);
		if(opcion == 1){
			printf("Cuanto desea sumar?:  ");
			scanf("%d", &aumento);
			contador += aumento;
		}
		printf("contador < 10 = %d < 10 = %s\n", contador, (contador < 10) ? "True" : "False");
	}
	
	// do-while('condición')
	// Se ejecuta el codigo y se verfica la condición al final
	// Si la condicion es falsa al inicio del codigo se ejcuta de 
	// igual manera pero solo una vez
	
	do {
		printf("contador = %d\n", contador);
		printf("Desea sumar al contador? (1 - Si, 2- No): ");
		scanf("%d", &opcion);
		if(opcion == 1){
			printf("Cuanto desea sumar?:  ");
			scanf("%d", &aumento);
			contador += aumento;
		}
		printf("contador < 10 = %d < 10 = %s\n", contador, (contador < 10) ? "True" : "False");
	} while (contador < 10);
	return 0;
}