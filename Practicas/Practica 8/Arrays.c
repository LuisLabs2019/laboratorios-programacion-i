#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	
	// Declaracion inicializada
	int array[] = {5, 4, 3, 2, 1};
	
	// Declaracion sin inicializar	
	int array[5];
	
	array[0] = 10;
	array[1] = 11;
	array[2] = 12;
	array[3] = 13;
	array[4] = 14;
	
	// Lectura con el teclado
	// scanf("%d", &array[0]);
	
	int i;
	
	for (i = 0; i < 5; i++){
		printf("array[%d] = %d\n", i, array[i]);
	}
	
	return 0;
}