#include <stdlib.h>
#include "SafeInput.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
// bit.ly/2uRDejz
int main(int argc, char *argv[]) {
	int num;
	int control = read_Int(&num);
	if(control){
		printf("%d\n", num);
	} else {
		printf("Error de entrada\n");
	}
	return 0;
}