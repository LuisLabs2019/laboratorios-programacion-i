# Laboratorio de Programación I (Programación estructurada en C)

## Datos del curso

**Nombre del instructor**: Luis Alberto Alonso Mendoza

**Correo**: la.labs2019@gmail.com

**Horario del curso**: 20:00 - 22:00 / Jueves

**Libro de apoyo**: https://drive.google.com/open?id=10ZpMXruJtFp1Ym4EQ9GtV0TUS5gE2lHH

**Enlace recortado del repositorio**: http://bit.ly/LabLuisPI2019

**Enlace de compilador en linea**: https://www.onlinegdb.com/online_c_compiler

## Criterios de evaluación e instrucciones

### Instrucciones

* El laboratorio consistirá en 14 prácticas que se realizarán en 14 semanas.
* La calificación final del laboratorio consistira en el promedio de las 14 
  prácticas, de acuerdo al porcentaje asigando con cada práctica.
* Es importante considerar que **el no asistir a la práctica amerita un 0 (cero)**
  de calificación en la misma, salvo en casos especiales que serán considerados
  de manera personal.
* No se permite la entrada después de **30 minutos** a la clase de laboratorio.
* Solo se justifican las faltas por motivos de fuerza mayor. Por ningún motivo
  se justificarán faltas por actividades extracurriculares (deportes, actividades
  culturales, etc.).
* Cada programa realizado como parte del laboratorio deberá guardarse en una 
  carpeta que tenga como nombre el numero de la práctica. Para cada programa
  debemos tener guardados el(los) archivos fuente y todos los archivos que se
  generen al compilar nuestro programa. Es importante tener una copia en USB de
  los programas realizados en el laboratorio para tener un respaldo
* Es libre la utilización de cualquier programa para escribir el código, 
  compilarlo y ejecutarlo, pero queda como labor del alumno aprender a usarlo 
  correctamente.
* Todas las prácticas realizadas (programas fuente), debidamente guardadas en 
  sus carpetas correspondientes, seran almacenadas en un CD que se entregará 
  al final del curso en las fechas fijadas por el instructor de laboratorio. 
  Es importante que el CD este rotulado con los siguientes datos:
    * Nombre del alumno
    * Matrícula
    * Unidad de aprendizaje
    * Día y hora del laboratorio
    * Grupo
    * Nombre del instructor de laboratorio
    * Nombre del maestro encargado de la materia
* Junto con el CD se entregará el portafolio de evidencias, la entrega de ambos 
  será requisito indispensable para acreditar el laboratorio. El portafolio de 
  evidencias debe de tener los mismos datos que se piden en el CD.
* Es importante atender al reglamento aquí especificado y si surge alguna duda 
  o inconveniente aclararlo con el instructor y de ser necesario con el maestro 
  titular de la materia.
* Antes de empezar a trabajar en el equipo de cómputo deberan de verificar que 
  no hay anormalidad en éste, en caso contrario deberá notificarlo de inmediato 
  al instructor.
* Abstenerse de instalar, borrar programas de aplicación o almacenar archivos en 
  los equipos de cómputo.
* Una vez que haya terminado de utilizar el equipo de cómputo deberá verificar 
  que el equipo y mobiliario estén en orden y dejar limpio su área de trabajo 
  al salir del laboratorio.
* Son responsables de verificar que lso archivos que traiga estén libres de 
  virus u otros programas nocivos, siendo obligatorio el escaneo de su 
  memoria USB.
* Para tener derecho a examen extraordianrio, debió haber cumplido con el 70%
  de sus prácticas.

### Formato de prácticas

Las prácticas deberán ser enviadas al correo especificado en la sección de 
arriba con el siguiente formato para el asunto:

**Formato del asunto del correo**: LPI_[Matricula]_P[No. Practica]

Las prácticas deben de ser entregadas el mismo día del laboratorio a menos 
que se indique lo contrario.

Los archivos que hayan escrito deberán ser comprimidos en un **"zip"** o
**"rar"** y adjuntados en el correo junto con el archivo Word de la
practica en cuestión.

En caso de que se detecte copia las prácticas de todos los involucrados
quedaran invalidas.

El no cumplir con alguna de las condiciones mencionadas la practica quedara
invalidada.

## Temario

1. Presentación                             0%
2. Expresiones                              3%
3. Programación básica                      3%
4. **"if"** e **"if else"**                 5%
5. **"if"** anidados y **"switch"**         5%
6. Ciclo **"for"**                          5%
7. Ciclos **"while"** y **"do while"**      7%
8. Arreglos de una dimensión (vectores)     9%
9. Arreglos de dos dimensiones (matrices)   9%
10. Cadenas y arreglos de cadenas           8%
11. Estructuras                             12%
12. Funciones                               12%
13. Punteros I                              6%
14. Punteros II                             6%
15. Archivos                                10%

## Asistencias y calificaciones
En la carpeta raíz de este repositorio se encontrará un archivo de texto 
llamado ---- donde se podrán encontrar los registros de asistencias y las 
calificaciones de cada una de las prácticas.